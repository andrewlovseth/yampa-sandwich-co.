<?php

/*

	Template Name: Catering

*/

get_header(); ?>


	<section id="page-header">
		<div class="wrapper">

			<div class="info">
				<h1><?php echo get_field('page_title'); ?></h1>
				<h2><?php echo get_field('page_headline'); ?></h2>

				<div class="info-wrapper">

					<div class="details">
						<div class="copy">
							<?php echo get_field('page_deck'); ?>
						</div>
						
						<div class="pdfs">
							<?php if(have_rows('pdfs')): while(have_rows('pdfs')): the_row(); ?>
		 
							    <div class="doc">
							    	<a href="<?php echo get_sub_field('document'); ?>" rel="external"><span><?php echo get_sub_field('filename'); ?></span></a>
							    </div>

							<?php endwhile; endif; ?>
						</div>						
					</div>
					
					<?php if(get_field('show_promo')): ?>

						<div class="promo">
							<div class="flag">
								<img src="<?php bloginfo('template_directory') ?>/images/flag-icon.svg" alt="Flag">
							</div>

							<div class="promo-info">
								<h4><?php echo get_field('promo_headline'); ?></h4>
								<a href="<?php echo get_field('promo_link'); ?>">
									<span><?php echo get_field('promo_link_label'); ?></span>
								</a>
							</div>
							
						</div>

					<?php endif; ?>

				</div>

			</div>

		</div>
	</section>

	<section id="menu">
		<div class="wrapper">

			<div class="jump-nav">

				<h4>Jump To</h4>

				<div class="jump-nav-wrapper">
			
					<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>

					    <?php if( get_row_layout() == 'overview'): ?>
							
							<div class="link">
					    		<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>"><?php echo get_sub_field('section_title'); ?></a>
							</div>
							
					    <?php endif; ?>
					 
					<?php endwhile; endif; ?>

				</div>

			</div>

			<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
 
			    <?php if( get_row_layout() == 'overview' ): ?>

				    <section class="overview menu-section" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>">
				    	<div class="header">
				    		<h4><span><?php echo get_sub_field('section_title'); ?></span></h4>
				    	</div>

				    	<div class="photo">
				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

						<div class="info">
				    		<?php echo get_sub_field('copy'); ?>
						</div>
			    	
				    </section>
					
			    <?php endif; ?>

			<?php endwhile; endif; ?>

		</div>
	</section>


<?php get_footer(); ?>