<?php

// Enqueue custom styles and scripts
function enqueue_styles_and_scripts() {
    // Register and noConflict jQuery 3.2.1
    wp_register_script( 'jquery.3.2.1', 'https://code.jquery.com/jquery-3.2.1.min.js' );
    wp_add_inline_script( 'jquery.3.2.1', 'var jQuery = $.noConflict(true);' );

    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    $script_last_updated_at = filemtime($dir . '/js/site.js' );
    $style_last_updated_at = filemtime($dir . '/style.css' );

    // Add style.css
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400,400i,600,700,900' );
    wp_enqueue_style( 'aos-styles', 'https://unpkg.com/aos@2.3.1/dist/aos.css' );
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css' , '', $style_last_updated_at);

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'aos-script', 'https://unpkg.com/aos@2.3.1/dist/aos.js');
    wp_enqueue_script( 'custom-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true );
    wp_enqueue_script( 'custom-site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts' );