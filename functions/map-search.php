<?php

function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyChPenuSzix17EHU8NezWBRia9ow8K_V8k';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


function enqueue_search_filter_ajax_scripts() {
  wp_register_script('search-filter', get_bloginfo('template_url') . '/js/zip-filter.js', '', true );
  wp_localize_script('search-filter', 'ajax_listing_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
  wp_enqueue_script('search-filter', get_bloginfo('template_url') . '/js/zip-filter.js','','',true);
}
add_action('wp_enqueue_scripts', 'enqueue_search_filter_ajax_scripts');

//Add Ajax Actions
add_action('wp_ajax_search_filter', 'ajax_search_filter');
add_action('wp_ajax_nopriv_search_filter', 'ajax_search_filter');

function ajax_search_filter() {

    get_template_part('partials/locations/ajax-map-view');

    wp_reset_postdata();

    die();
}
