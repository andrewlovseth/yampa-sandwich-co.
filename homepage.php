<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<a href="#" class="next-prev" id="slide-prev"></a>
	<a href="#" class="next-prev" id="slide-next"></a>

	<div class="slideshow">

		<?php if(have_rows('slideshow')): while(have_rows('slideshow')): the_row(); ?>


			<div class="slide-wrapper">
				<section class="banner">
					<div class="wrapper">

						<div class="label">
							<h5><?php echo get_sub_field('banner_label'); ?></h5>
						</div>

						<div class="info">
							<div class="headline">
								<h1><?php echo get_sub_field('banner_headline'); ?></h1>
							</div>

							<div class="subheadline">
								<h2><?php echo get_sub_field('banner_subheadline'); ?></h2>
							</div>
						</div>	

					</div>
				</section>

				<section class="hero">
					<div class="wrapper">

						<a href="<?php echo get_sub_field('order_link'); ?>" class="flag-btn"><?php echo get_sub_field('order_link_label'); ?></a>
						 
					    <div class="slide">
					    	<div class="photo">
						    	<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    	</div>

					        <div class="info">
					        	<h3><?php echo get_sub_field('headline'); ?></h3>
					        	<h4><?php echo get_sub_field('subheadline'); ?></h4>	
					        </div>
					    </div>
						
					</div>
				</section>		
			</div>
			
		<?php endwhile; endif; ?>
		
	</div>


	<section id="email-sign-up">
		<div class="wrapper">

			<div class="info">

				<h3><?php echo get_field('email_headline'); ?></h3>
				<p><?php echo get_field('email_deck'); ?></p>

				<div class="form">
					<?php
						$shortcode = get_field('email_form_shortcode');
						echo do_shortcode($shortcode);
					?>					
				</div>

				<div class="notice">
					<p><?php echo get_field('email_notice'); ?></p>
				</div>
			</div>

			<div class="photo">
				<img src="<?php $image = get_field('email_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />				
			</div>
			
		</div>
	</section>


	<section id="our-story" class="cover" style="background-image: url(<?php $image = get_field('our_story_background_image'); echo $image['url']; ?>);">
		<div class="wrapper">
			
			<div class="info">
				<?php get_template_part('partials/green-dots'); ?>
				<h2><?php echo get_field('our_story_headline'); ?></h2>
				<p><?php echo get_field('our_story_deck'); ?></p>

				<a href="<?php echo site_url('/our-story/'); ?>" class="flag-btn"><?php echo get_field('our_story_cta_label'); ?></a>
			</div>


		</div>
	</section>


	<section id="franchise">
		<div class="wrapper">
			
			<div class="info">
				<?php get_template_part('partials/green-dots'); ?>
				<h2><?php echo get_field('franchise_headline'); ?></h2>
				<p><?php echo get_field('franchise_deck'); ?></p>
				
				<?php 
					$link = get_field('franchise_link');
					if( $link ): 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				 ?>

					<div class="cta">
						<a class="flag-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
					</div>

				<?php endif; ?>
			</div>

			<div class="gallery">
				<?php  $images = get_field('franchise_photos'); if( $images ): ?>
					<?php foreach( $images as $image ): ?>
						<div class="photo">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>

		</div>
	</section>


<?php get_footer(); ?>