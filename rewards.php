<?php

/*

	Template Name: Rewards

*/

get_header(); ?>

	<section id="page-header">
		<div class="wrapper">

			<div class="info">
				<h1><?php echo get_field('page_title'); ?></h1>
				<h2><?php echo get_field('page_headline'); ?></h2>

				<div class="info-wrapper">
					<div class="details">
						<div class="copy">
							<?php echo get_field('page_deck'); ?>
						</div>									
					</div>
				</div>

			</div>

		</div>
	</section>


	<section id="rewards">
		<div class="wrapper">

			<div class="loyalty-card">
				<h3><?php echo get_field('loyalty_card_headline'); ?></h3>
				<?php echo get_field('loyalty_card_deck'); ?>

				<div class="graphic">
					<img src="<?php $image = get_field('loyalty_card_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="sign-in">
					<h4><?php echo get_field('loyalty_card_sign_in_headline'); ?></h4>
					<a href="<?php echo get_field('loyalty_card_sign_in_link'); ?>"><?php echo get_field('loyalty_card_sign_in_link_label'); ?></a>
				</div>

			</div>
			
			<div class="newsletter">
				<h3><?php echo get_field('newsletter_headline'); ?></h3>
				<?php echo get_field('newsletter_deck'); ?>

				<div class="form">
					<?php
						$shortcode = get_field('newsletter_form');
						echo do_shortcode($shortcode);
					?>					
				</div>

				<div class="notice">
					<p><?php echo get_field('newsletter_form_note'); ?></p>
				</div>
			</div>

		</div>
	</section>

<?php get_footer(); ?>