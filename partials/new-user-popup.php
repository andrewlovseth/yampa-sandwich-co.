<?php if(get_field('show_popup', 'options')): ?>

	<section id="new-user">
		<div class="overlay">
			<div class="overlay-wrapper">

				<a href="#" class="close">✕</a>

				<div class="headline">
					<h1><?php echo get_field('popup_headline', 'options'); ?></h1>
				</div>

				<div class="copy">
					<?php echo get_field('popup_copy', 'options'); ?>
				</div>

				<?php $link = get_field('popup_cta', 'options'); if( $link ): ?>
					<div class="cta">
						<?php
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a class="flag-btn green" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					</div>
				<?php endif; ?>

				<?php if(get_field('popup_close_label', 'options')): ?>
					<div class="confirm">
						<a href="#" class="flag-btn close-modal"><?php echo get_field('popup_close_label', 'options'); ?></a>
					</div>
				<?php endif; ?>
		
			</div>
		</div>
	</section>

<?php endif; ?>