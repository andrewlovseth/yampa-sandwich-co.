<?php if(have_rows('links')): ?>
    
    <?php while(have_rows('links')) : the_row(); ?>

        <?php if( get_row_layout() == 'external_link' ): ?>

            <?php 
                $link = get_sub_field('link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="doc external-link">
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                        <span><?php echo esc_html($link_title); ?></span>
                    </a>
                </div>

            <?php endif; ?>

        <?php endif; ?>

        <?php if( get_row_layout() == 'pdf' ): ?>

            <?php 
                $file = get_sub_field('file');
                if( $file ): 
            ?>

                <div class="doc">
                    <a href="<?php echo esc_url($file['url']); ?>" target="window">
                        <span><?php echo esc_html($file['title']); ?></span>
                    </a>
                </div>

            <?php endif; ?>

        <?php endif; ?>

    <?php endwhile; ?>

<?php endif; ?>