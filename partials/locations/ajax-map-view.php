<?php

$query_data = $_GET;
$zip_search = $query_data['searchTerm'];

function getZipLatLon($zip){
	$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyApmCb4YZZp0x92dCMVtLqyCfyzyiTzS74&address=" . urlencode($zip);
	$response = wp_remote_get($url);
	$api_response = json_decode( wp_remote_retrieve_body( $response ), true );
	$results = $api_response['results'];

	$geometry[] = $results[0]['geometry'];
	$location[] = $geometry[0]['location'];
	return $location;
}

function getDistance($zip1, $zip2){
	$first_zip = getZipLatLon($zip1);
	$second_zip = getZipLatLon($zip2);

	$lat1 = $first_zip[0]['lat'];
	$lon1 = $first_zip[0]['lng'];
	$lat2 = $second_zip[0]['lat'];
	$lon2 = $second_zip[0]['lng'];

	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;

	return $miles;
}

$results = array();

if($zip_search != '') {

	$zip_args = array(
		'post_type' => 'stores',
		'posts_per_page' => 250
	);
	$zip_query = new WP_Query( $zip_args  );

	if ( $zip_query->have_posts() ) : while ( $zip_query->have_posts() ) : $zip_query->the_post(); 

		$store_zip = get_field('zip_code');
		$distance = getDistance($zip_search, $store_zip);
		$proximity = 25;

		if ( intval($distance) < intval($proximity) ) {
			$results[] = array("id" => get_the_id(), "distance" => $distance);
		}			

	endwhile; endif; wp_reset_postdata();
}

array_multisort(array_column($results, 'distance'), SORT_ASC, SORT_NUMERIC, $results);
$sorted_results = array();
$sorted_results = array_column($results, 'id');

$search_args = array(
	'post_type' => 'stores',
	'posts_per_page' => 250,
	'post__in' => $sorted_results,
	'orderby' => 'post__in'
);

$search_loop = new WP_Query($search_args);

$sorted_results = array_filter($sorted_results);

if ( empty($sorted_results) && !empty($zip_search != '') ): ?>

	<div class="no-results">
		<h3>Looks like the nearest location is more than 25 miles away.</h3>		
	</div>

<?php endif;

if( $search_loop->have_posts() ): ?>

	<div class="map-view-wrapper">				
		<div class="sidebar">
			
			<?php while( $search_loop->have_posts() ): $search_loop->the_post(); ?>

				<?php get_template_part('partials/locations/store-sidebar'); ?>

			<?php endwhile; ?>

		</div>

		<style type="text/css">

			.acf-map {
				width: 100%;
				height: 500px;
			}

			.acf-map img {
			   max-width: inherit !important;
			}
		</style>

		<script type="text/javascript">
			(function($) {

			/*
			*  new_map
			*
			*  This function will render a Google Map onto the selected jQuery element
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	4.3.0
			*
			*  @param	$el (jQuery element)
			*  @return	n/a
			*/

			function new_map( $el ) {
				
				// var
				var $markers = $el.find('.marker');
				
				
				// vars
				var args = {
					zoom		: 16,
					center		: new google.maps.LatLng(0, 0),
					mapTypeId	: google.maps.MapTypeId.ROADMAP,
					disableDefaultUI: true,
					zoomControl: true,
					styles: 
						[
						    {
						        "featureType": "administrative",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "color": "#3b1431"
						            }
						        ]
						    },
						    {
						        "featureType": "landscape",
						        "elementType": "all",
						        "stylers": [
						            {
						                "color": "#e0efef"
						            },
						            {
						                "lightness": "-6"
						            },
						            {
						                "gamma": "1.00"
						            }
						        ]
						    },
						    {
						        "featureType": "landscape.man_made",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#e9e5dc"
						            }
						        ]
						    },
						    {
						        "featureType": "landscape.natural",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#e0efef"
						            }
						        ]
						    },
						    {
						        "featureType": "landscape.natural",
						        "elementType": "labels",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "landscape.natural.terrain",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#c0e8e8"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "all",
						        "stylers": [
						            {
						                "visibility": "simplified"
						            },
						            {
						                "color": "#f2f6f8"
						            },
						            {
						                "lightness": "-10"
						            },
						            {
						                "gamma": "1.00"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "labels.icon",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "poi.park",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#c0e8e8"
						            }
						        ]
						    },
						    {
						        "featureType": "road",
						        "elementType": "all",
						        "stylers": [
						            {
						                "saturation": -100
						            },
						            {
						                "lightness": 45
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "elementType": "all",
						        "stylers": [
						            {
						                "visibility": "simplified"
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "elementType": "labels.icon",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "transit",
						        "elementType": "all",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "water",
						        "elementType": "all",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            },
						            {
						                "visibility": "on"
						            }
						        ]
						    }
						]
				};
				
				
				// create map	        	
				var map = new google.maps.Map( $el[0], args);
				
				
				// add a markers reference
				map.markers = [];
				
				
				// add markers
				$markers.each(function(){
					
			    	add_marker( $(this), map );
					
				});
				
				
				// center map
				center_map( map );
				
				
				// return
				return map;
				
			}


			function center_map( map ) {

				// vars
				var bounds = new google.maps.LatLngBounds();

				// loop through all markers and create bounds
				$.each( map.markers, function( i, marker ){

					var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

					bounds.extend( latlng );

				});

				// only 1 marker?
				if( map.markers.length == 1 )
				{
					// set center of map
				    map.setCenter( bounds.getCenter() );
				    map.setZoom( 16 );
				}
				else
				{
					// fit to bounds
					map.fitBounds( bounds );
				}

			}

			/*
			*  add_marker
			*
			*  This function will add a marker to the selected Google Map
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	4.3.0
			*
			*  @param	$marker (jQuery element)
			*  @param	map (Google Map object)
			*  @return	n/a
			*/

			function add_marker( $marker, map ) {

				// var
				var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

				// create marker
				var marker = new google.maps.Marker({
					position	: latlng,
					map			: map,
					icon		: '<?php bloginfo('template_directory') ?>/images/map-pin.png'
				});

				// add to array
				map.markers.push( marker );

				// if marker contains HTML, add it to an infoWindow
				if( $marker.html() )
				{
					// create info window
					var infowindow = new google.maps.InfoWindow({
						content		: $marker.html()
					});

					// show info window when marker is clicked
					google.maps.event.addListener(marker, 'click', function() {

						infowindow.open( map, marker );

					});
				}

			}



			/*
			*  document ready
			*
			*  This function will render each map when the document is ready (page has loaded)
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	5.0.0
			*
			*  @param	n/a
			*  @return	n/a
			*/
			// global var
			var map = null;

			$(document).ready(function(){

				$('.acf-map').each(function(){

					// create map
					map = new_map( $(this) );

				});

				// Smooth Scroll
				$('a.smooth').smoothScroll();

			});

			})(jQuery);
		</script>

		<div class="acf-map">

			<?php while( $search_loop->have_posts() ): $search_loop->the_post(); ?>

				<?php get_template_part('partials/locations/store-pin'); ?>

			<?php endwhile; ?>

		</div>				
	</div>

<?php endif; wp_reset_postdata(); die(); ?>