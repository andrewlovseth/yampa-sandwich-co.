<div class="store">
	<h5><?php the_title(); ?></h5>
	<div class="contact">
		<p class="address"><?php echo get_field('address'); ?></p>
		<p class="phone"><a href="tel:<?php echo get_field('phone'); ?>"><?php echo get_field('phone'); ?></a></p>
	</div>

	<div class="links">
		<div class="details">
			<a href="#<?php echo sanitize_title_with_dashes(get_the_title()); ?>" class="smooth">Details</a>
		</div>

		<div class="directions">
			<a href="<?php echo get_field('google_maps_link'); ?>" rel="external">Directions</a>
		</div>
	</div>
</div>