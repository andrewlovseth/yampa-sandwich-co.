<?php $location = get_field('map'); ?>

<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
	<h5><a href="#<?php echo sanitize_title_with_dashes(get_the_title()); ?>" class="smooth"><?php the_title(); ?></a></h5>
</div>