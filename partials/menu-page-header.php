<?php

	$menu_page = get_page_by_path('menu');
	$menu = $menu_page->ID; 

?>

<section id="page-header">
	<div class="wrapper">

		<div class="info">
			<h1><?php echo get_field('page_title', $menu); ?></h1>
			<h2><?php echo get_field('page_headline', $menu); ?></h2>

			<div class="info-wrapper">

				<div class="details">
					<div class="copy">
						<?php echo get_field('page_deck', $menu); ?>
					</div>
					
					<div class="pdfs">
						<?php get_template_part('partials/menu-links'); ?>
					</div>						
				</div>
				
				<?php if(get_field('show_promo', $menu)): ?>

					<div class="promo">
						<div class="flag">
							<img src="<?php bloginfo('template_directory') ?>/images/flag-icon.svg" alt="Flag">
						</div>

						<div class="promo-info">
							<h4><?php echo get_field('promo_headline', $menu); ?></h4>
							<a href="<?php echo get_field('promo_link', $menu); ?>">
								<span><?php echo get_field('promo_link_label', $menu); ?></span>
							</a>
						</div>
						
					</div>

				<?php endif; ?>

			</div>

		</div>

	</div>
</section>


<section id="menu-subnav">
	<div class="wrapper">
		
		<div class="mobile">
			<?php if(have_rows('sub_nav', $menu)): while(have_rows('sub_nav', $menu)): the_row(); ?>
		 
			    <div class="link">
			    	<a href="<?php echo get_sub_field('link'); ?>">
			    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    		<span class="page-title"><?php echo get_sub_field('label'); ?></span>  		
			    	</a>        
			    </div>

			<?php endwhile; endif; ?>			
		</div>


		<div class="desktop">
			<?php if(have_rows('sub_nav', $menu)): while(have_rows('sub_nav', $menu)): the_row(); ?>
		 
			    <div class="link<?php if(get_sub_field('button_style')): ?> btn<?php endif; ?>">
			    	<a href="<?php echo get_sub_field('link'); ?>" class="<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?> <?php $page_id = get_sub_field('link', false, false); if(is_page($page_id)): ?>active<?php endif; ?>">
			    		<?php echo get_sub_field('label'); ?>    		
			    	</a>        
			    </div>

			<?php endwhile; endif; ?>			
		</div>	

	</div>
</section>