<?php

/*

	Template Name: Catering 2020 

*/

get_header(); ?>


	<section id="page-header">
		<div class="wrapper">

			<div class="info">
				<h1><?php echo get_field('page_title'); ?></h1>
				<h2><?php echo get_field('page_headline'); ?></h2>

				<div class="info-wrapper">

					<div class="details">
						<div class="copy">
							<?php echo get_field('page_deck'); ?>
						</div>
						
						<div class="pdfs">
							<?php get_template_part('partials/menu-links'); ?>
						</div>						
					</div>
					
					<?php if(get_field('show_promo')): ?>

						<div class="promo">
							<div class="flag">
								<img src="<?php bloginfo('template_directory') ?>/images/flag-icon.svg" alt="Flag">
							</div>

							<div class="promo-info">
								<h4><?php echo get_field('promo_headline'); ?></h4>
								<a href="<?php echo get_field('promo_link'); ?>">
									<span><?php echo get_field('promo_link_label'); ?></span>
								</a>
							</div>
							
						</div>

					<?php endif; ?>

				</div>

			</div>

		</div>
	</section>

	<section id="menu">
		<div class="wrapper">

			<div class="jump-nav">

				<h4>Jump To</h4>

				<div class="jump-nav-wrapper">
			
					<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>

					    <?php if( get_row_layout() == 'overview' || 'popup' ): ?>
							
							<div class="link">
					    		<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>"><?php echo get_sub_field('section_title'); ?></a>
							</div>
							
					    <?php endif; ?>
					 
					<?php endwhile; endif; ?>

				</div>

			</div>

			<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
 
			    <?php if( get_row_layout() == 'overview' ): ?>

				    <section class="overview menu-section" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>">
				    	<div class="header">
				    		<h4><span><?php echo get_sub_field('section_title'); ?></span></h4>
				    	</div>

				    	<div class="photo">
				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

						<div class="info">
				    		<?php echo get_sub_field('copy'); ?>
						</div>
			    	
				    </section>
					
			    <?php endif; ?>


			    <?php if( get_row_layout() == 'popup' ): ?>

				    <section class="overview menu-section popup" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>">
				    	<div class="header">
				    		<h4><span><?php echo get_sub_field('section_title'); ?></span></h4>
				    	</div>

				    	<div class="photo">
				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

						<div class="info">
							<div class="note">
								<?php echo get_sub_field('note'); ?>
							</div>

							<div class="offerings">
								<?php if(have_rows('offerings')): while(have_rows('offerings')): the_row(); ?>
								 
								    <div class="offering">
								    	<div class="headline">
								    		<h3><?php echo get_sub_field('title'); ?></h3>
								    	</div>

								    	<div class="copy">
								    		<?php echo get_sub_field('copy'); ?>
								    	</div>

								    	<div class="options">
									    	<?php if(have_rows('options')): while(have_rows('options')): the_row(); ?>
											 
											    <div class="option">
											    	<div class="link">
														<?php 
														$link = get_sub_field('link');
														if( $link ): 
														    $link_url = $link['url'];
														    $link_title = $link['title'];
														    ?>
														    <a class="option-trigger" href="<?php echo esc_url( $link_url ); ?>"><span><?php echo get_sub_field('name'); ?></span></a>
														<?php endif; ?>
											    	</div>

											    	<div class="price">
											    		<span><?php echo get_sub_field('price'); ?></span>
											    	</div>
											    </div>

											<?php endwhile; endif; ?>								    		
								    	</div>								        
								    </div>

								<?php endwhile; endif; ?>
							</div>				    		
						</div>

						<?php if(have_rows('popups')): while(have_rows('popups')): the_row(); ?>
						 
						    <div class="popup-info" id="<?php echo get_sub_field('id'); ?>">
						    	<div class="popup-wrapper">
					    			<a href="#" class="close close-x option-close">✕</a>

							    	<div class="header">
							    		<h2><?php echo get_sub_field('header'); ?></h2>
							    	</div>

							    	<?php if(get_sub_field('options_note')): ?>
							    		<div class="items-note">
							    			<?php echo get_sub_field('options_note'); ?>
							    		</div>
							    	<?php endif; ?>

							    	<div class="items">
							    		<?php if(have_rows('options')): while(have_rows('options')): the_row(); ?>
										 
										    <div class="item">
										    	<p>
										    		<span class="name"><?php echo get_sub_field('name'); ?></span>
										    		<?php if(get_sub_field('ingredients')): ?>
											    		<span class="ingredients"><?php echo get_sub_field('ingredients'); ?></span>
											    	<?php endif; ?>
										    	</p>										        
										    </div>

										<?php endwhile; endif; ?>							    		
							    	</div>

									<div class="footer">
										<div class="got-it">
											<a href="#" class="option-close close flag-btn">Got It!</a>
										</div>
									</div>
							    </div>
						    </div>

						<?php endwhile; endif; ?>
			    	
				    </section>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>


<?php get_footer(); ?>