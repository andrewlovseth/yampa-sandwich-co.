<?php

/*

	Template Name: Philanthropy

*/

get_header(); ?>

	<section id="page-header">
		<div class="wrapper">

			<div class="info">
				<h1><?php echo get_field('page_title'); ?></h1>
				<h2><?php echo get_field('page_headline'); ?></h2>

				<div class="info-wrapper">

					<div class="details">
						<div class="copy">
							<?php echo get_field('page_deck'); ?>
						</div>					
					</div>
					
					<?php if(get_field('show_promo')): ?>

						<div class="promo">
							<div class="flag">
								<img src="<?php bloginfo('template_directory') ?>/images/flag-icon.svg" alt="Flag">
							</div>

							<div class="promo-info">
								<h4><?php echo get_field('promo_headline'); ?></h4>
								<a href="<?php echo get_field('promo_link'); ?>" class="smooth">
									<span><?php echo get_field('promo_link_label'); ?></span>
								</a>
							</div>
							
						</div>

					<?php endif; ?>

				</div>

			</div>

		</div>
	</section>


	<section id="causes">
		<div class="wrapper">

			<div class="copy">
				<?php echo get_field('causes_copy'); ?>
			</div>

		</div>
	</section>


	<section id="contact-form">
		<div class="wrapper">

			<div class="headline">
				<h3><?php echo get_field('contact_form_headline'); ?></h3>
			</div>

			<div class="copy">
				<?php echo get_field('contact_form_copy'); ?>
			</div>

			<div class="contact-form">
				<?php
					$shortcode = get_field('contact_form_shortcode');
					echo do_shortcode($shortcode);
				?>
			</div>


		</div>
	</section>



<?php get_footer(); ?>