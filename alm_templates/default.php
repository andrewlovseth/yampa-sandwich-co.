<div class="cause">
	<div class="headline">
		<h4><a href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('name'); ?></a></h4>
	</div>

	<div class="copy">
    	<h5>Why?</h5>
	    <?php echo get_sub_field('copy'); ?>
	</div>

</div>