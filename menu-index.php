<?php

/*

	Template Name: Menu Index

*/

get_header(); ?>

	<?php get_template_part('partials/menu-page-header'); ?>



    <section class='mobile-pdfs'>
        <div class="wrapper">

			<?php get_template_part('partials/menu-links'); ?>

		</div>
	</section>


	<div id="ajax">

		<section id="menu">
			<div class="wrapper">

				<?php $main_menu = get_page_by_path('menu/specialty-sandwiches'); ?>

				<div class="jump-nav">

					<h4>Jump To</h4>

					<div class="jump-nav-wrapper">
						<a href="#" class="close">✕</a>
				
						<?php if(have_rows('sections', $main_menu )): while(have_rows('sections', $main_menu )) : the_row(); ?>

							    <?php if( get_row_layout() == 'items' || 'overview' ): ?>
									
									<div class="link">
							    		<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>"><?php echo get_sub_field('section_title'); ?></a>
									</div>
									
							    <?php endif; ?>
						 
						<?php endwhile; endif; ?>

					</div>

				</div>


				<?php if(have_rows('sections', $main_menu)): while(have_rows('sections', $main_menu)) : the_row(); ?>

				    <?php if( get_row_layout() == 'items' ): ?>
						
					    <section class="items menu-section" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>">
					    	
					    	<div class="header">
					    		<h4><span><?php echo get_sub_field('section_title'); ?></span></h4>
					    	</div>

					    	<div class="food-items">
								<?php $food = get_sub_field('food_items'); if( $food ): ?>
									<?php foreach( $food as $p ):?>
									    <article>
											<div class="headline">
												<h3><?php echo get_the_title( $p->ID ); ?></h3>

												<?php if(get_field('try_it_toasted', $p->ID)): ?>
													<div class="try-it-toasted">
														<img src="<?php bloginfo('template_directory') ?>/images/toasted-icon.svg" alt="Try It Toasted">
													</div>
												<?php endif; ?>
											</div>
											<div class="photo">
												<img src="<?php $image = get_field('photo', $p->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</div>

											<div class="info">
												<?php echo get_field('description', $p->ID); ?>

												<a href="#nutrition-<?php echo sanitize_title_with_dashes(get_the_title($p->ID)); ?>" class="view-link">Nutrition Info</a>

												<div class="nutrition-info" id="nutrition-<?php echo sanitize_title_with_dashes(get_the_title($p->ID)); ?>">
													<div class="overlay-wrapper">
														<a href="#" class="close x">✕</a>

														<h3><?php echo get_the_title( $p->ID ); ?></h3>
														<h4>Nutrition Information</h4>

														<div class="row calories">
															<div class="key">
																<span class="label">Calories</span>
															</div>
															<div class="value">
																<span class="number"><?php echo get_field('calories', $p->ID); ?></span>
															</div>
														</div>

														<?php if(have_rows('nutrition_information', $p->ID)): while(have_rows('nutrition_information', $p->ID)): the_row(); ?>

															<div class="row">
																<div class="key">
																	<span class="label"><?php echo get_sub_field('label'); ?></span>
																</div>
																<div class="value">
																	<span class="number"><?php echo get_sub_field('value'); ?></span>
																</div>
															</div>

														<?php endwhile; endif; ?>

														<div class="footer">

															<?php if(get_field('contains', $p->ID)): ?>
																<div class="contains">
																	<h5>Contains</h5>
																	<p><?php echo get_field('contains', $p->ID); ?></p>
																</div>
															<?php endif; ?>

															<div class="got-it">
																<a href="#" class="close flag-btn">Got It!</a>
															</div>
															
														</div>

													</div>
												</div>
											</div>

									    </article>
									<?php endforeach; ?>
								<?php endif; ?>				    		
					    	</div>
					    </section>
						
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'overview' ): ?>

					    <section class="overview menu-section" id="<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>">
					    	<div class="header">
					    		<h4><span><?php echo get_sub_field('section_title'); ?></span></h4>
					    	</div>

					    	<div class="photo">
					    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    	</div>

							<div class="info">
					    		<?php echo get_sub_field('copy'); ?>
							</div>
				    	
					    </section>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>
				
			</div>
		</section>

	</div>


<?php get_footer(); ?>