<?php

/*

	Template Name: Franchise

*/

get_header(); ?>

	<section id="page-header">
		<div class="wrapper">

			<div class="info">
				<h1><?php echo get_field('page_title'); ?></h1>
				<h2><?php echo get_field('page_headline'); ?></h2>

				<div class="info-wrapper">

					<div class="details">
						<div class="copy">
							<?php echo get_field('page_deck'); ?>
						</div>
						
						<div class="pdfs">
							<?php if(have_rows('pdfs')): while(have_rows('pdfs')): the_row(); ?>
		 
							    <div class="doc">
							    	<a href="<?php echo get_sub_field('document'); ?>" rel="external"><span><?php echo get_sub_field('filename'); ?></span></a>
							    </div>

							<?php endwhile; endif; ?>
						</div>						
					</div>
					
					<?php if(get_field('show_promo')): ?>

						<div class="promo">
							<div class="flag">
								<img src="<?php bloginfo('template_directory') ?>/images/flag-icon.svg" alt="Flag">
							</div>

							<div class="promo-info">
								<h4><?php echo get_field('promo_headline'); ?></h4>
								<a href="<?php echo get_field('promo_link'); ?>">
									<span><?php echo get_field('promo_link_label'); ?></span>
								</a>
							</div>
							
						</div>

					<?php endif; ?>

				</div>

			</div>

		</div>
	</section>


	<section id="menu">
		<div class="wrapper">

			<div class="jump-nav">

				<h4>Jump To</h4>

				<div class="jump-nav-wrapper">
					<?php if(have_rows('jump_nav')): while(have_rows('jump_nav')): the_row(); ?>
 
						<div class="link">
				    		<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('section_title')); ?>"><?php echo get_sub_field('section_title'); ?></a>
						</div>

					<?php endwhile; endif; ?>
				</div>

			</div>

		</div>
	</section>


	<section id="our-story">
		<div class="wrapper">

			<div class="headline">
				<h3><?php echo get_field('our_story_headline'); ?></h3>
			</div>

			<div class="copy">
				<?php echo get_field('our_story_copy'); ?>
			</div>

			<div class="photo shadow">
				<img src="<?php $image = get_field('our_story_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
			
		</div>
	</section>


	<section id="about">
		<div class="wrapper">
			
			<div class="info">
				<div class="headline">
					<h4><?php echo get_field('about_headline'); ?></h4>
				</div>

				<div class="copy">
					<?php echo get_field('about_copy'); ?>
				</div>
			</div>

			<div class="photo shadow">
				<img src="<?php $image = get_field('about_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

		</div>
	</section>


	<section id="about-features" class="cover">
		<div class="wrapper">

			<?php if(have_rows('about_features')): while(have_rows('about_features')): the_row(); ?>
			 
			    <div class="feature">
					<div class="headline">
						<h4><?php echo get_sub_field('headline'); ?></h4>
					</div>

					<div class="copy">
						<?php echo get_sub_field('copy'); ?>
					</div>
			    </div>

			<?php endwhile; endif; ?>

			<?php $images = get_field('about_gallery'); if( $images ): ?>
				<div class="gallery">
					<?php foreach( $images as $image ): ?>
						<div class="photo">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			
		</div>
	</section>


	<section id="the-opportunity">
		<div class="wrapper">
			
			<div class="headline">
				<h3><?php echo get_field('opportunity_headline'); ?></h3>
			</div>

			<div class="copy">
				<?php echo get_field('opportunity_copy'); ?>
			</div>

			<div class="details">
				<div class="info">
					<?php echo get_field('opportunity_details'); ?>
				</div>

				<div class="photo shadow">
					<img src="<?php $image = get_field('opportunity_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>

		</div>
	</section>


	<section id="revenue-streams">
		<div class="wrapper">

			<div class="headline">
				<h4><?php echo get_field('revenue_streams_headline'); ?></h4>
			</div>

			<div class="features">
				<?php if(have_rows('revenue_streams_features')): while(have_rows('revenue_streams_features')): the_row(); ?>
				 
				    <div class="feature">
				    	<?php echo get_sub_field('copy'); ?>
				    </div>

				<?php endwhile; endif; ?>
			</div>		

		</div>
	</section>


	<section id="support">
		<div class="wrapper">
			
			<div class="headline">
				<h4><?php echo get_field('support_headline'); ?></h4>
			</div>

			<div class="copy">
				<?php echo get_field('support_copy'); ?>
			</div>

			<div class="features">
				<?php if(have_rows('support_features')): while(have_rows('support_features')): the_row(); ?>
				 
				    <div class="feature">
				    	<h4><?php echo get_sub_field('headline'); ?></h4>
				    	<?php echo get_sub_field('copy'); ?>
				    </div>

				<?php endwhile; endif; ?>
			</div>		

		</div>
	</section>


	<section id="faqs">
		<div class="wrapper">
	
			<div class="headline">
				<h3><?php echo get_field('faqs_headline'); ?></h3>
			</div>

			<div class="copy">
				<?php echo get_field('faqs_copy'); ?>
			</div>

			<div class="pdfs">
				<?php if(have_rows('faqs_pdfs')): while(have_rows('faqs_pdfs')): the_row(); ?>

				    <div class="doc">
				    	<a href="<?php echo get_sub_field('document'); ?>"><span><?php echo get_sub_field('filename'); ?></span></a>
				    </div>

				<?php endwhile; endif; ?>
			</div>	

			<div class="faqs-list">

				<?php if(have_rows('faqs_list')): while(have_rows('faqs_list')): the_row(); ?>
				 
				    <div class="faq">
				    	<div class="question">
				    		<h4><?php echo get_sub_field('question'); ?></h4>
				    	</div>

				    	<div class="answer">
				    		<?php echo get_sub_field('answer'); ?>
				    	</div>
				    </div>

				<?php endwhile; endif; ?>
				
			</div>

		</div>
	</section>


	<section id="get-started">
		<div class="wrapper">

			<div class="headline">
				<h4><?php echo get_field('get_started_headline'); ?></h4>
			</div>

			<div class="copy">
				<?php echo get_field('get_started_copy'); ?>
			</div>

			<div class="contact-form">
				<?php
					$shortcode = get_field('get_started_shortcode');
					echo do_shortcode($shortcode);
				?>
			</div>

			<div class="disclosures">
				<?php echo get_field('disclosures'); ?>
			</div>


		</div>
	</section>

<?php get_footer(); ?>