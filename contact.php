<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	<section id="page-header">
		<div class="wrapper">

			<div class="info">
				<h1><?php echo get_field('page_title'); ?></h1>
				<h2><?php echo get_field('page_headline'); ?></h2>

				<div class="info-wrapper">
					<div class="details">
						<div class="copy">
							<?php echo get_field('page_deck'); ?>
						</div>									
					</div>
				</div>

			</div>

		</div>
	</section>


	<section id="contact">
		<div class="wrapper">

			<div class="contact-form">
				<?php
					$shortcode = get_field('contact_form');
					echo do_shortcode($shortcode);
				?>	
			</div>


			<div class="community">
				<h3><?php echo get_field('community_headline'); ?></h3>

				<img src="<?php $image = get_field('community_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				
				<?php echo get_field('community_copy'); ?>
			</div>

		</div>
	</section>


<?php get_footer(); ?>