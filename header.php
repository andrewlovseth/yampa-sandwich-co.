<!DOCTYPE html>
<html>
<head>
	<meta name="google-site-verification" content="-BxLa55R7XZ4EZ4nAhngO0nwyfSdbd-ycnNwMBcmRAQ" />
	
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header>
		<div class="wrapper">

			<div class="header-wrapper">
				
				<div class="logo">
					<a href="<?php echo site_url('/'); ?>">
						<span class="mobile">
							<img src="<?php $image = get_field('mobile_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</span>

						<span class="desktop active">
							<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</span>
					</a>
				</div>

				<a href="#" id="toggle">
					<div class="patty"></div>
				</a>

				<nav>
					<div class="nav-wrapper">
						<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
	 						
							<?php 
								$link = get_sub_field('link');
								if( $link ): 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
							 ?>

								<div class="link <?php echo sanitize_title_with_dashes($link_title); ?>">
									<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
										<?php echo esc_html($link_title); ?>
									</a>
								</div>

							<?php endif; ?>

						<?php endwhile; endif; ?>						
					</div>
				</nav>

				<div class="order-online">
					<a href="<?php echo get_field('order_online_link', 'options'); ?>" rel="external">
						<span class="icon"><img src="<?php $image = get_field('order_online_icon', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></span>
						<span class="label">
							<span class="mobile"><?php echo get_field('order_online_mobile_label', 'options'); ?></span>
							<span class="desktop"><?php echo get_field('order_online_desktop_label', 'options'); ?></span>
						</span>
					</a>
				</div>

			</div>

		</div>
	</header>