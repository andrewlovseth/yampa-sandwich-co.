<?php get_header(); ?>

	<section id="page-header" class="error-404">
		<div class="wrapper">

			<div class="info">
				<h1>404</h1>
				<h2><?php echo get_field('404_headline', 'options'); ?></h2>

				<div class="info-wrapper">
					<div class="details">
						<div class="copy">
							<?php echo get_field('404_copy', 'options'); ?>
						</div>

						<div class="image">
							<img src="<?php $image = get_field('404_image', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>							
					</div>
				</div>
			</div>

		</div>
	</section>
	   	
<?php get_footer(); ?>