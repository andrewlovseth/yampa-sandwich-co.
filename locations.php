<?php

/*

	Template Name: Locations

*/

get_header(); ?>

	<section id="page-header">
		<div class="wrapper">

			<div class="info">
				<h1><?php echo get_field('page_title'); ?></h1>
				<h2><?php echo get_field('page_headline'); ?></h2>

				<div class="info-wrapper">
					<div class="details">
						<div class="copy">
							<?php echo get_field('page_deck'); ?>
						</div>									
					</div>

					<div class="promo desktop">
						<div class="promo-info cover"  style="background-image: url(<?php $image = get_field('promo_background_photo'); echo $image['url']; ?>);">
							<h4><?php echo get_field('promo_headline'); ?></h4>
							<a href="<?php echo get_field('promo_link'); ?>">
								<span><?php echo get_field('promo_link_label'); ?></span>
							</a>

							<div class="communities">
								<a href="<?php echo get_field('community_link'); ?>">
									<span><?php echo get_field('community_link_label'); ?></span>
								</a>
							</div>

						</div>
					</div>

				</div>
			</div>

		</div>
	</section>


	<section id="map-view">
		<div class="wrapper">

			<div class="ajax">
					
				<form class="zip-code-filter">
					<label>Find a location near you (Within 25 miles):</label>

					<div class="input">
						<input type="text" id="zip" placeholder="Enter a city or zip code" />
					</div>
					<div class="submit">
						<input type="submit" value="Search" />
					</div>
				</form>

				<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChPenuSzix17EHU8NezWBRia9ow8K_V8k"></script>

				<div id="response"></div>

			</div>

		</div>
	</section>

	<section class="mobile-promo">
		
		<div class="promo">
			<div class="promo-info cover">
				<h4><?php echo get_field('promo_headline'); ?></h4>
				<a href="<?php echo get_field('promo_link'); ?>">
					<span><?php echo get_field('promo_link_label'); ?></span>
				</a>

				<div class="communities">
					<a href="<?php echo get_field('community_link'); ?>">
						<span><?php echo get_field('community_link_label'); ?></span>
					</a>
				</div>

			</div>
		</div>

	</section>


	<section id="list-view">
		<div class="wrapper">

			<div class="info">
				<?php get_template_part('partials/green-dots'); ?>
				<h2>Location Details</h2>
			</div>

			<div class="stores">

				<?php
					$args = array(
						'post_type' => 'stores',
						'posts_per_page' => 50
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<div class="store" id="<?php echo sanitize_title_with_dashes(get_the_title()); ?>">
						<div class="header">
							<h3><?php the_title(); ?></h3>
							<p class="address"><?php echo get_field('address'); ?></p>
						</div>
						
						<div class="contact">
							<p class="hours"><?php echo get_field('hours'); ?></p>
							<p class="phone"><a href="tel:<?php echo get_field('phone'); ?>"><?php echo get_field('phone'); ?></a></p>
						</div>

						<div class="photo">
							<div class="order">
								<a href="<?php echo get_field('order_online'); ?>" class="flag-btn" rel="external">Order Online</a>
							</div>
							<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>

					</div>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</div>
			
		</div>
	</section>

<?php get_footer(); ?>