	<footer>
		<div class="wrapper">

			<div class="footer-wrapper">
					
				<div class="footer-nav">
					<?php if(have_rows('footer_navigation', 'options')): while(have_rows('footer_navigation', 'options')): the_row(); ?>
							
						<?php 
							$link = get_sub_field('link');
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						 ?>

							<div class="link">
								<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
							</div>

						<?php endif; ?>

					<?php endwhile; endif; ?>		
				</div>

				<div class="social-nav">
					<?php if(have_rows('social_navigation', 'options')): while(have_rows('social_navigation', 'options')): the_row(); ?>
							
						<div class="link">
						    <a href="<?php echo get_sub_field('link'); ?>" rel="external">
						        <img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						    </a>	 							
						</div>

					<?php endwhile; endif; ?>
				</div>

				<div class="copyright">
					<?php echo get_field('copyright', 'options'); ?>
				</div>

			</div>

		</div>
	</footer>

	<?php get_template_part('partials/new-user-popup'); ?>

	<script>
	  AOS.init();
	</script>	
	
	<?php wp_footer(); ?>

	<?php echo get_field('js_snippets', 'options'); ?>

</body>
</html>