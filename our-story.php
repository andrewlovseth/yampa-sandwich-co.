<?php

/*

	Template Name: Our Story

*/

get_header(); ?>

	<section id="page-header">
		<div class="wrapper">

			<div class="info">
				<h1><?php echo get_field('page_title'); ?></h1>
				<h2><?php echo get_field('page_headline'); ?></h2>

				<div class="info-wrapper">
					<div class="details">
						<div class="copy">
							<?php echo get_field('page_deck'); ?>
						</div>									
					</div>

					<div class="promo" style="background-image: url(<?php $image = get_field('community_background_photo'); echo $image['url']; ?>);">
						<div class="promo-info">
							<h4><?php echo get_field('community_headline'); ?></h4>
							<a href="<?php echo get_field('community_link'); ?>">
								<span><?php echo get_field('community_link_label'); ?></span>
							</a>
						</div>				
					</div>
				</div>
			</div>

		</div>
	</section>


	<section id="mission" class="cover" style="background-image: url(<?php $image = get_field('mission_background_photo'); echo $image['url']; ?>);">
		<div class="wrapper">
			<div class="photo">
				<img src="<?php $image = get_field('mission_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="info">
				<h3><?php echo get_field('mission_headline'); ?></h3>
				<?php echo get_field('mission_copy'); ?>
			</div>
			
		</div>
	</section>

	<section class="promo mobile-promo" style="background-image: url(<?php $image = get_field('community_background_photo'); echo $image['url']; ?>);">
		<div class="promo-info">
			<h4><?php echo get_field('community_headline'); ?></h4>
			<a href="<?php echo get_field('community_link'); ?>">
				<span><?php echo get_field('community_link_label'); ?></span>
			</a>
		</div>				
	</section>


	<section id="name">
		<div class="wrapper">
			
			<div class="info">
				<?php get_template_part('partials/green-dots'); ?>
				<h2><?php echo get_field('name_headline'); ?></h2>
				<?php echo get_field('name_copy'); ?>
			</div>
		
		</div>
	</section>


	<section id="sandwich">
		<div class="wrapper">
		
			<div class="header">
				<h4><?php echo get_field('sandwich_headline'); ?></h4>
				<?php echo get_field('sandwich_copy'); ?>
			</div>

			<div class="stack">

				<?php if(have_rows('sandwich_stack')): $count = 100; while(have_rows('sandwich_stack')):  the_row(); ?>
				 
				    <div class="layer" data-aos="fade-up" style="z-index: <?php echo $count; ?>">
				    	<div class="info">
							<a href="#" class="close">✕</a>
					        <h5><?php echo get_sub_field('header'); ?></h5>

					        <div class="line-wrapper">
					        	<div class="line"></div>
					        	<div class="dot"></div>
					    	</div>

					        <p><?php echo get_sub_field('deck'); ?></p>
				    	</div>

				    	<div class="photo">
				    		<div class="dot">				    			
				    		</div>

				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

				    	<div class="gutter"></div>
				    </div>

				<?php --$count; endwhile; endif; ?>
				
			</div>

		</div>
	</section>


<?php get_footer(); ?>