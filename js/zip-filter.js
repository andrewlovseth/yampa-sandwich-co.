jQuery(function($) {

	//Load posts on page load
	search_get_posts();

	//Find Search Term
	function getSearchTerm() {
		var zip = $('#zip');
		if (zip.value == '' || zip.value == zip.defaultValue) {
			var searchTerm = $(zip).val();
			return searchTerm;
		}
	}

	//On sort/filter change
	$('.zip-code-filter').on('submit', function(){
		search_get_posts();
		return false;
	});
	
	//Main ajax function
	function search_get_posts() {
		var ajax_url = ajax_listing_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'search_filter',
				searchTerm: getSearchTerm
			},
			beforeSend: function () {
				//Show loader here
			},
			success: function(data) {
				//Hide loader here
				$('#response').html(data);
			},
			error: function() {
				$("#response").html('<p>There has been an error</p>');
			}
		});				
	}
	
});