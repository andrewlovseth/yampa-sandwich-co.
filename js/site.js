(function ($, window, document, undefined) {

	$(document).ready(function() {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});


		$('.page-template-menu-index .promo-info a, .page-template-catering .promo-info a').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});


		// Menu Toggle
		$('#toggle').click(function(){
			$('header, nav').toggleClass('open');
			return false;
		});


		// Home Slideshow
		$('body.home .slideshow').slick({
			dots: true,
			prevArrow: $('#slide-prev'),
			nextArrow: $('#slide-next'),
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			slidesToScroll: 1,
			mobileFirst: true,
			autoplay: true,
			autoplaySpeed: 5000,
		});

		$('#slide-prev').prependTo('.slick-dots');
		$('#slide-next').appendTo('.slick-dots');

		// Smooth Scroll
		$('a.smooth').smoothScroll();


		// Smooth Scroll to Menu Section
		$('.jump-nav .link a').smoothScroll({offset: -250});


		// Mobile Jump Nav Show
		$('.jump-nav h4').on('click', function(){
			$('.jump-nav-wrapper').toggleClass('modal').toggle();
		});


		// Mobile Jump Nav Hide
		$('.jump-nav .close').on('click', function(){
			$('.jump-nav-wrapper').toggleClass('modal').toggle();
		});


		// Mobile Sandwich Modal Show
		$('.layer .photo .dot').on('click', function(){
			var thisInfo = $(this).closest('.photo').siblings('.info');
			$(thisInfo).toggleClass('modal').toggle();
		});


		// Mobile Sandwich Modal Hide
		$('.layer .info .close').on('click', function(){
			var thisInfo = $(this).closest('.info');
			$(thisInfo).toggleClass('modal').toggle();

			return false;
		});



		// Show Nutrition Info	
		$('.food-items article .view-link').on('click', function(){
			$(this).siblings('.nutrition-info').addClass('show');

			return false;
		});


		// Hide Nutrition Info
		$('.nutrition-info .close').on('click', function(){
			$(this).closest('.nutrition-info').removeClass('show');

			return false;
		});




		// Show Catering Popup	
		$('.option-trigger').on('click', function(){
			var	target = $(this).attr('href');

			$('body').addClass('catering-popup');

			$(target).addClass('show');

			return false;
		});


		// Hide Catering Popup
		$('.option-close').on('click', function(){
			$(this).closest('.popup-info').removeClass('show');
			$('body').removeClass('catering-popup');

			return false;
		});



		$('body.page-template-menu-index .desktop .link:not(.btn) a').on('click', function() {

			var href = $(this).attr('href');

			$('#ajax #menu').fadeOut(300, function(){
			    $('#ajax #menu').load(href + ' #menu .wrapper', function(){
			        $('#ajax #menu').fadeIn(300);
			    });
			});

			$('body.page-template-menu-index .desktop .link:not(.btn) a').removeClass('active');
			$(this).addClass('active');

			return false;
		}).filter('.specialty-sandwiches').click();;


		// FAQ Accordion
		$('.faq .question').on('click', function(){

			var question = $(this);
			var answer = $(question).siblings('.answer');

			$(question).toggleClass('open');
			$(answer).slideToggle(200);

		});


		// New User Popup
	    var showModal = localStorage.getItem('showModal');

		if (sessionStorage.pageCount) {
		  sessionStorage.pageCount = Number(sessionStorage.pageCount) + 1;
		} else {
		  sessionStorage.pageCount = 1;
		}

		if(sessionStorage.pageCount == 1) {

		    if(showModal == null) {

		        localStorage.setItem('showModal', 1);
		        $('#new-user').fadeIn('slow');

		    } else if(showModal >= 1 && showModal <= 2) {

		    	var visit_count = parseInt(localStorage.getItem('showModal'));
		    	visit_count++;
		        localStorage.setItem('showModal', visit_count);	        
		        $('#new-user').fadeIn('slow');

		    } else {
		    	
		    	var visit_count = parseInt(localStorage.getItem('showModal'));
		    	visit_count++;
		        localStorage.setItem('showModal', visit_count);	

		    }
		}




		$('#new-user a.close, #new-user a.flag-btn.close-modal').on('click', function(){
			$('#new-user .overlay').addClass('hide');
			setTimeout(function() {
				$('#new-user').hide();
			}, 500);
			return false;
		});


		// Sticky Bits
		$('#menu-subnav').stickybits();

			
	});

	$(document).keyup(function(e) {
	     if (e.key === "Escape") { // escape key maps to keycode `27`

			$('#new-user .overlay').addClass('hide');
			setTimeout(function() {
				$('#new-user').hide();
			}, 500);
			return false;


	    }
	});



	$( document ).ajaxComplete(function( event, xhr, settings ) {

		// rel="external"
		$('.sidebar .directions a').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});


		// Smooth Scroll
		$('a.smooth').smoothScroll();

		// Smooth Scroll to Menu Section
		$('.jump-nav .link a').smoothScroll({offset: -150});

		// Show Nutrition Info	
		$('.food-items article .view-link').on('click', function(){
			$(this).siblings('.nutrition-info').addClass('show');

			return false;
		});
		// Hide Nutrition Info
		$('.nutrition-info .close').on('click', function(){
			$(this).closest('.nutrition-info').removeClass('show');

			return false;
		});

	});

})(jQuery, window, document);