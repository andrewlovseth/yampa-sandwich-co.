<?php get_header(); ?>

	<section id="page-header">
		<div class="wrapper">

			<div class="info">
				<h1><?php the_title(); ?></h1>
			</div>


		</div>
	</section>

	<section class="generic">
		<div class="wrapper">

			<?php the_content(); ?>

		</div>
	</section>

<?php get_footer(); ?>